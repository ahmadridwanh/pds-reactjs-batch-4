//Nomor 1
console.log('Nomor 1');

for(var i = 0; i < 10; i++){
    console.log( + i );
}

console.log('-----------')

//Nomor 2
console.log('Nomor 2');

for(var i = 1; i <= 10; i++){
    if((i%2)==1){
      console.log(i);
  }
}

console.log('-----------');

//Nomor 3
console.log('Nomor 3');

for (var i=0; i<=9; i+=2)
console.log(i);

console.log('-----------');

//Nomor 4
console.log('Nomor 4');
let array1 = [1,2,3,4,5,6];

console.log(array1[5]);
console.log('-----------');

//Nomor 5
console.log('Nomor 5');
let array2 = [5,2,4,1,3,5];
let urut = array2.sort();

console.log(urut);
console.log('-----------');

//Nomor 6
console.log('Nomor 6');
let array3 = ["selamat", "anda", "melakukan", "perulangan", "array", "dengan", "for"];

for(var i = 0; i < array3.length; i++) {
    console.log(array3[i]);
   }

console.log('-----------');

//Nomor 7
console.log('Nomor 7');
let array4 = [1, 2, 3, 4, 5, 6,7, 8, 9, 10];

for(var i = 0; i <= array4.length; i++){
    if((i%2)==1){
console.log(array4[i]);
 }
}

console.log('-----------');

//Nomor 8
console.log('Nomor 8');

let kalimat= ["saya", "sangat", "senang", "belajar", "javascript"];
let text = kalimat.join(' ');

console.log(text);

console.log('-----------');

//Nomor 9
console.log('Nomor 9');

var sayuran=[];
var eksekusi = sayuran.push('kangkung','bayam','buncis','kubis','timun','seledri','tauge');

console.log(sayuran);

console.log('-----------');