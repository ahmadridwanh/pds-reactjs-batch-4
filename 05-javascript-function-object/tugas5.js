console.log("---Nomor 1---")
function cetakFunction() {
    return "halo nama saya ridwan";
  }
   
  console.log(cetakFunction())

  console.log("---Nomor 2---")
let angka1 = 20
let angka2 = 7

let output = myFunction(angka1, angka2)

function myFunction(angka1, angka2) {
    return angka1 + angka2
  }

console.log(output)

console.log("---Nomor 3---")
const Hello = () => {
    console.log("Hello")
}

Hello()

console.log("---Nomor 4---")
let obj = {

    nama : "john",
    
    umur : 22,
    
    bahasa : "indonesia"
    
    }
console.log(obj.bahasa)

console.log("---Nomor 5---")
let arrayDaftarPeserta = ["John Doe", "laki-laki", "baca buku" , 1992]
let objDaftarPeserta = {
    "nama" : arrayDaftarPeserta[0],
    "Jenis kelamin" : arrayDaftarPeserta[1],
    "Hobi" : arrayDaftarPeserta[2],
    "Tahun" : arrayDaftarPeserta[3]

}

console.log("Nama : " + arrayDaftarPeserta[0])
console.log("Jenis kelamin : " + arrayDaftarPeserta[1])
console.log("Hobi : " + arrayDaftarPeserta[2])
console.log("Tahun : " + arrayDaftarPeserta[3])

console.log("---Nomor 6---")
const buah = [
    { nama: 'Nanas', warna : 'Kuning', adaBijinya : 'false', harganya : 9000 },
    { nama: 'Jeruk', warna : 'Orange', adaBijinya : 'true', harganya : 8000 },
    { nama: 'Semangka', warna : 'Hijau & Merah', adaBijinya : 'false', harganya : 10000 },
    { nama: 'Pisang', warna : 'Kuning', adaBijinya : 'false', harganya : 5000 },
  ];
  
  const filterbijinya = buah.filter(bijinya => bijinya.adaBijinya == 'false');
  
  console.log(filterbijinya);

  console.log("---Nomor 7---")
  let phone = {
    brand: "Samsung",
    name: "Galaxy Fold 4",
    year: 2023
 }
  
  let { brand, name, year } = phone;
  
  console.log(brand, name, year);

  console.log("---Nomor 8---")
  let dataBukuTambahan= {
    penulis: "john doe",
    tahunTerbit: 2020 
  }
  
  let buku = {
    nama: "pemograman dasar",
    jumlahHalaman: 172
  }
  
  let objOutput = {
    penulis : "John doe", tahunTerbit : 2020, nama : "pemrograman dasar", jumlahHalaman : 172
  }

  console.log(objOutput)

  console.log("---Nomor 9---")
  let mobil = {
    merk : "bmw",
    color: "red",
    year : 2002
    }
    
    const functionObject = (param) => {
    
    return param
    
    }

console.log(functionObject(mobil))