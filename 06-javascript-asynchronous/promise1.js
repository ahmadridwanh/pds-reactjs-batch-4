function myCountPromise(a, b = 2) {
  return new Promise((resolve, reject) => {
    if (typeof a !== 'number' || typeof b !== 'number') {
      reject('Maaf tidak ada nilai dalam parameter');
    } else {
      setTimeout(function() {
        console.log("saya dijalankan belakangan")
      }, 2000)
      const result = a * b;
      resolve(result);
    }
  });
}

myCountPromise(2)
  .then(result => {
    console.log(result);
  })
  .catch(error => {
    console.log(error);
  });

  function filterBooksPromise(colorful, amountOfPage) {
    return new Promise(function(resolve, reject) {
      var books = [
        { name: "shinchan", totalPage: 50, isColorful: true },
        { name: "Kalkulus", totalPage: 250, isColorful: false },
        { name: "doraemon", totalPage: 40, isColorful: true },
        { name: "algoritma", totalPage: 250, isColorful: false },
      ];
  
      if (amountOfPage >= 40) {
        resolve(books.filter(x => x.totalPage >= amountOfPage && x.isColorful === colorful));
      } else {
        var reason = "Maaf buku di bawah 40 halaman tidak tersedia";
        reject(reason);
      }
    });
  }
  
  async function runFilterBooksPromise() {
    try {
      var result1 = await filterBooksPromise(true, 40);
      console.log("Buku berwarna dan jumlah halaman 40:");
      console.log(result1);
  
      var result2 = await filterBooksPromise(false, 250);
      console.log("Buku tidak berwarna dan jumlah halaman 250:");
      console.log(result2);
  
      var result3 = await filterBooksPromise(true, 30);
      console.log("Buku berwarna dan jumlah halaman 30:");
      console.log(result3);
    } catch (error) {
      console.log(error);
    }
  }
  
  runFilterBooksPromise();
  